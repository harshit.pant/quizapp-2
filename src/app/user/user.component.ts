import { Component, OnInit } from '@angular/core';
import {UserService} from '../user.service';
import { FormGroup, FormBuilder, Validators, FormControl, FormArray } from '@angular/forms';



@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  quizform:FormGroup;
  questionarray:FormArray;
  public quiz=[];
  public selectedItem=[];
  constructor(private service:UserService) { }

  ngOnInit(): void {
    this.service.getquiz().subscribe((res)=>{
      this.quiz = [];
      for(var i=0;i<res.length;i++){
        this.quiz.push(res[i].payload.doc.data())
      }
      for(var i=0;i<res.length;i++){
        this.quiz[i].id=res[i].payload.doc.id
      }
    })

  }
  onSelect(item){

    this.selectedItem = item;
    console.log(item);


  }


}
