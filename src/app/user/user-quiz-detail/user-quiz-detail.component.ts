import { Component, OnInit, Input } from '@angular/core';

@Component({
	selector: 'app-user-quiz-detail',
	templateUrl: './user-quiz-detail.component.html',
	styleUrls: ['./user-quiz-detail.component.css']
})
export class UserQuizDetailComponent implements OnInit {

	@Input() selectedItem;
	public answers = {};

	constructor() { }

	ngOnInit(): void { }

	selectedanswer(index, answer) {
		console.log(index);
		console.log(answer);
		this.answers[index] = answer;
	}

	onSubmit() {
		var scored_value = 0;
		console.log(this.selectedItem);
		for (let index = 0; index < this.selectedItem.questionarray.length; index++) {
			if (this.selectedItem.questionarray[index].correctoption === this.answers[index]) {
				scored_value++;
			}
		}
		console.log(scored_value);
		alert('you secured ' + scored_value + ' out of ' + this.selectedItem.questionarray.length);
		this.answers = {};
	}

}
