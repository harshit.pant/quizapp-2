import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { firestore } from 'firebase/app';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private _firestore: AngularFirestore) { }

  getquiz(){
    return this._firestore.collection('quiz').snapshotChanges();
  }

}
