import { Component, OnInit, EventEmitter, Input } from '@angular/core';
import {UserService} from '../../user.service'
@Component({
  selector: 'app-user-quiz-list',
  templateUrl: './user-quiz-list.component.html',
  styleUrls: ['./user-quiz-list.component.css'],
  outputs:['selectQuiz']
})
export class UserQuizListComponent implements OnInit {
  public quiz =[];
  public hidelist:boolean=false;
    public selectQuiz = new EventEmitter();

  constructor(private _service:UserService) { }

  ngOnInit(): void {
    this._service.getquiz().subscribe((res)=>{
      this.quiz = [];
      for(var i=0;i<res.length;i++){
        this.quiz.push(res[i].payload.doc.data())
      }
      for(var i=0;i<res.length;i++){
        this.quiz[i].id=res[i].payload.doc.id
      }
      console.log(this.quiz)
    })

  }
  onSelect(quize){
    this.hidelist=true;
    this.selectQuiz.emit(quize);
    console.log(quize)
  }


}
