import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl, FormArray } from '@angular/forms';
import {AdminService} from '../admin.service';


@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {
quizform:FormGroup;
questionarray:FormArray;
// users:array;
  constructor(private service:AdminService,  private _fb:FormBuilder) { }

  ngOnInit(): void {
    this.quizform = this._fb.group({
    url:'',
    text:'',
    titleName: ['',Validators.required],
    description:['',Validators.required],
    questionarray: this._fb.array([ this.createQuestion() ])
    });
  }

  createQuestion(): FormGroup {
    return this._fb.group({
      question: new FormControl(""),
      option1: new FormControl (""),
      option2: new FormControl (""),
      option3: new FormControl (""),
      option4: new FormControl (""),
      correctoption: new FormControl ("")
    });
  }

  addQuestion(): void {
    this.questionarray = (<FormArray>this.quizform.get('questionarray')) as FormArray;
    this.questionarray.push(this.createQuestion());
   }
  

  onSubmit() {
    this.service.addNewQuiz(this.quizform);
    console.log(this.quizform.value);
    alert('new quiz added')
    this.quizform.reset();
  }



}
