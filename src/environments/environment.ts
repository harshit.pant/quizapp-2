// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyCKcTOebOi9ryoeYUO5qQULmIEzV7cQqec",
    authDomain: "mockquizapp.firebaseapp.com",
    databaseURL: "https://mockquizapp.firebaseio.com",
    projectId: "mockquizapp",
    storageBucket: "mockquizapp.appspot.com",
    messagingSenderId: "862844727878",
    appId: "1:862844727878:web:d4be06763a487cf088d047"
}
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
