import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { firestore } from 'firebase/app';


@Injectable({
  providedIn: 'root'
})
export class AdminService {

  constructor(private _firestore: AngularFirestore) { }
  addNewQuiz(data){
    return new Promise<any>((resolve, reject) =>{
        this._firestore
            .collection("quiz")
            .add(data.value)
            .then(res => {console.log('added '+ res)}, err => reject(err));
    });
  }

}
